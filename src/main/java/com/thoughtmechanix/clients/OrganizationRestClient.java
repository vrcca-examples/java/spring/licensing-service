package com.thoughtmechanix.clients;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.thoughtmechanix.model.Organization;
import com.thoughtmechanix.repository.OrganizationRedisRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Component
public class OrganizationRestClient {

    private static final Logger logger = LoggerFactory.getLogger(OrganizationRestClient.class);

    private final RestTemplate restTemplate;
    private final OrganizationRedisRepository redisRepository;

    @Autowired
    public OrganizationRestClient(RestTemplate restTemplate, OrganizationRedisRepository redisRepository) {
        this.restTemplate = restTemplate;
        this.redisRepository = redisRepository;
    }

    @HystrixCommand(commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")
    })
    public Organization getOrganization(String organizationId) {
        logger.debug("Trying to retrieve organization id {}", organizationId);
        return retrieveFromCache(organizationId)
                .or(() -> retrieveFromService(organizationId).map(this::saveToCache))
                .orElse(null);
    }

    @NewSpan("readLicensingDataFromRedis")
    private Optional<Organization> retrieveFromCache(@SpanTag String organizationId) {
        return redisRepository.findOrganization(organizationId);
    }

    private Optional<Organization> retrieveFromService(String organizationId) {
        return Optional.ofNullable(getForEntity(organizationId).getBody());
    }

    private ResponseEntity<Organization> getForEntity(String organizationId) {
        try {
            logger.debug("Requesting in organization-service...");
            return restTemplate
                    .getForEntity(
                            "http://zuul-server/api/organization/v1/organizations/{organizationId}",
                            Organization.class,
                            organizationId);
        } catch (Exception e) {
            logger.error("Error reaching organization-service {}", e);
            throw e;
        }
    }

    private Organization saveToCache(Organization organization) {
        redisRepository.saveOrganization(organization);
        return organization;
    }
}
