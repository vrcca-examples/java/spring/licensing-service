package com.thoughtmechanix.repository;

import com.thoughtmechanix.model.Organization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Repository
public class OrganizationRedisRepositoryImpl implements OrganizationRedisRepository {

    private static final String HASH_NAME = "organization";
    private static final Logger logger = LoggerFactory.getLogger(OrganizationRedisRepositoryImpl.class);

    private final RedisTemplate<String, Object> redisTemplate;
    private HashOperations<String, String, Organization> hashOperations;

    public OrganizationRedisRepositoryImpl(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @PostConstruct
    public void init() {
        hashOperations = redisTemplate.opsForHash();
    }

    @Override
    public void saveOrganization(Organization org) {
        try {
            hashOperations.put(HASH_NAME, org.getId(), org);
        } catch (Exception ex) {
            logger.error("Error encountered while trying to save organization {}. " +
                    "Check Redis cache." +
                    "Exception: {}", org, ex);
        }
    }

    @Override
    public void updateOrganization(Organization org) {
        hashOperations.put(HASH_NAME, org.getId(), org);
    }

    @Override
    public void deleteOrganization(String organizationId) {
        hashOperations.delete(HASH_NAME, organizationId);
    }

    @Override
    public Optional<Organization> findOrganization(String organizationId) {
        try {
            final Optional<Organization> maybeOrganization = Optional.ofNullable(hashOperations.get(HASH_NAME, organizationId));
            maybeOrganization.ifPresentOrElse(
                    organization -> logRetrievedFromCache(organizationId, organization),
                    () -> logNoValueFound(organizationId));
            return maybeOrganization;
        } catch (Exception ex) {
            logger.error("Error encountered while trying to retrieve organization {}. " +
                    "Check Redis cache." +
                    "Exception: {}", organizationId, ex);
            return Optional.empty();
        }
    }

    private void logNoValueFound(String organizationId) {
        logger.debug("Unable to locate organization from the redis cache: {}", organizationId);
    }

    private void logRetrievedFromCache(String organizationId, Organization organization) {
        logger.debug("I have successfully retrieved an organization {} from the redis cache: {}", organizationId, organization);
    }
}
