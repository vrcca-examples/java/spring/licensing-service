package com.thoughtmechanix.repository;

import com.thoughtmechanix.model.Organization;

import java.util.Optional;

public interface OrganizationRedisRepository {
    void saveOrganization(Organization org);

    void updateOrganization(Organization org);

    void deleteOrganization(String organizationId);

    Optional<Organization> findOrganization(String organizationId);
}
