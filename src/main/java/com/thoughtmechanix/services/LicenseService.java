package com.thoughtmechanix.services;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.thoughtmechanix.clients.OrganizationRestClient;
import com.thoughtmechanix.model.License;
import com.thoughtmechanix.model.Organization;
import com.thoughtmechanix.repository.LicenseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class LicenseService {

    private final Logger logger = LoggerFactory.getLogger(LicenseService.class);

    private final LicenseRepository licenseRepository;
    private final OrganizationRestClient organizationService;

    @Autowired
    public LicenseService(LicenseRepository licenseRepository,
                          OrganizationRestClient organizationService) {
        this.licenseRepository = licenseRepository;
        this.organizationService = organizationService;
    }

    public License getLicense(String organizationId, String licenseId) {
        final var license = licenseRepository.findByOrganizationIdAndLicenseId(organizationId, licenseId);
        final var organization = retrieveOrganizationInfo(organizationId);
        return license.withOrganization(organization);
    }

    private Organization retrieveOrganizationInfo(String organizationId) {
        return organizationService.getOrganization(organizationId);
    }

    public License create(License newLicense) {
        final var organization = retrieveOrganizationInfo(newLicense.getOrganizationId());
        return licenseRepository.save(newLicense.withNewId().withOrganizationId(organization.getId()));
    }

    // WARNING: These configs should be in Spring Config for production
    @HystrixCommand(
            fallbackMethod = "retrieveByOrganizationIdFallback",
            threadPoolKey = "licenseByOrgThreadPool",
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize", value = "30"), // number of threads
                    @HystrixProperty(name = "maxQueueSize", value = "10") // queue size
            },
            commandProperties = {
                    // consecutive calls before hystrix consider it
                    @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"),
                    // percentage of errors to activate bulkhead (after volume threshold)
                    @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "75"),
                    // after the percentage of errors, sleep time before trying to reach out remote call
                    @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "7000"),
                    // the size window for hystrix to monitor for problems
                    @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "15000"),
                    // hystrix collects metrics in buckets, this value limits the buckets (15/5)
                    @HystrixProperty(name = "metrics.rollingStats.numBuckets", value = "5")
            }
    )
    public List<License> retrieveByOrganizationId(String organizationId) {
        logRequestsFor("retrieveByOrganizationId");
        final var organization = retrieveOrganizationInfo(organizationId);
        return findAllByOrganization(organization);
    }

    private void logRequestsFor(String operation) {
        logger.info("requesting {}", operation);
    }

    private List<License> findAllByOrganization(Organization organization) {
        final var licenses = licenseRepository.findByOrganizationId(organization.getId());
        return licenses
                .stream()
                .map(l -> l.withOrganization(organization))
                .collect(toList());
    }

    private List<License> retrieveByOrganizationIdFallback(String organizationId) {
        return Collections.singletonList(License
                .emptyLicense()
                .withId("0000000-00-00000")
                .withOrganizationId(organizationId)
                .withProductName("Sorry no licensing information currently available")
                .withComment("Could not be loaded.")
        );
    }
}
