package com.thoughtmechanix.events.model;

public class OrganizationChangeModel {
    private final String typeName;
    private final String action;
    private final String organizationId;
    private final String correlationId;

    public OrganizationChangeModel(String typeName, String action, String organizationId, String correlationId) {
        this.typeName = typeName;
        this.action = action;
        this.organizationId = organizationId;
        this.correlationId = correlationId;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getAction() {
        return action;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public String getCorrelationId() {
        return correlationId;
    }
}
