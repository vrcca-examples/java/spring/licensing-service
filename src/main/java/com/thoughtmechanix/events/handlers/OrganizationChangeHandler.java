package com.thoughtmechanix.events.handlers;

import com.thoughtmechanix.events.CustomChannels;
import com.thoughtmechanix.events.model.OrganizationChangeModel;
import com.thoughtmechanix.repository.OrganizationRedisRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@EnableBinding(CustomChannels.class)
public class OrganizationChangeHandler {

    private static final Logger logger = LoggerFactory.getLogger(OrganizationChangeHandler.class);

    private final OrganizationRedisRepository organizationRedisRepository;

    public OrganizationChangeHandler(OrganizationRedisRepository organizationRedisRepository) {
        this.organizationRedisRepository = organizationRedisRepository;
    }

    @StreamListener("inboundOrgChanges")
    public void loggerSink(OrganizationChangeModel changeModel) {
        final String eventType = changeModel.getAction();
        final String organizationId = changeModel.getOrganizationId();
        switch (eventType) {
            case "UPDATE":
                logger.info("Received an event {} for organization id {}", eventType, organizationId);
                organizationRedisRepository.deleteOrganization(organizationId);
                break;
            case "DELETE":
                logger.info("Received an event {} for organization id {}", eventType, organizationId);
                organizationRedisRepository.deleteOrganization(organizationId);
                break;
            default:
                logger.error("Received an UNKNOWN event of type {}", eventType);
                break;
        }
    }
}
