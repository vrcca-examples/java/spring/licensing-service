package com.thoughtmechanix.controllers;

import com.thoughtmechanix.model.License;
import com.thoughtmechanix.services.LicenseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v1/organizations/{organizationId}/licenses")
public class LicenseController {

    private final Logger logger = LoggerFactory.getLogger(LicenseController.class);
    private final LicenseService licenseService;

    @Autowired
    public LicenseController(LicenseService licenseService) {
        this.licenseService = licenseService;
    }

    @GetMapping("/{licenseId}")
    public License retrieve(@PathVariable("organizationId") String organizationId,
                            @PathVariable("licenseId") String licenseId) {
        logRequestsFor("retrieve");
        return licenseService.getLicense(organizationId, licenseId);
    }

    @GetMapping
    public List<License> listAllByOrganizationId(@PathVariable("organizationId") String organizationId) {
        logRequestsFor("listAllByOrganizationId");
        return licenseService.retrieveByOrganizationId(organizationId);
    }

    @PostMapping
    public License create(@PathVariable("organizationId") String organizationId,
                          @RequestBody License newLicense) {
        logRequestsFor("create");
        final String newLicenseId = licenseService.create(newLicense.withOrganizationId(organizationId)).getLicenseId();
        return retrieve(organizationId, newLicenseId);
    }

    private void logRequestsFor(String operation) {
        logger.info("Requesting {}", operation);
    }
}
