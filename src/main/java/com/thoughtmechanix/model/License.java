package com.thoughtmechanix.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.UUID;

@Entity
@Table(name = "licenses")
public class License {
    public static final License EMPTY_LICENSE = new License(
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null);

    @Id
    @Column(name = "license_id", nullable = false)
    private String licenseId;

    @Column(name = "organization_id", nullable = false)
    private String organizationId;

    @Column(name = "product_name", nullable = false)
    private String productName;

    @Column(name = "license_type", nullable = false)
    private String licenseType;

    @Column(name = "license_max", nullable = false)
    private Integer licenseMax;

    @Column(name = "license_allocated", nullable = false)
    private Integer licenseAllocated;


    @Transient
    private String comment;
    @Transient
    private String organizationName;
    @Transient
    private String contactName;
    @Transient
    private String contactEmail;
    @Transient
    private String contactPhone;

    private License() {
        // DO NOT DELETE: hibernate-only
    }

    public License(String licenseId,
                   String productName,
                   String licenseType,
                   String organizationId,
                   Integer licenseMax,
                   Integer licenseAllocated,
                   String comment,
                   String organizationName,
                   String contactName,
                   String contactEmail,
                   String contactPhone) {
        this.licenseId = licenseId;
        this.productName = productName;
        this.licenseType = licenseType;
        this.organizationId = organizationId;
        this.licenseMax = licenseMax;
        this.licenseAllocated = licenseAllocated;
        this.comment = comment;
        this.organizationName = organizationName;
        this.contactName = contactName;
        this.contactEmail = contactEmail;
        this.contactPhone = contactPhone;
    }


    public static License emptyLicense() {
        return EMPTY_LICENSE;
    }

    public String getLicenseId() {
        return licenseId;
    }

    public String getProductName() {
        return productName;
    }

    public String getLicenseType() {
        return licenseType;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public Integer getLicenseMax() {
        return licenseMax;
    }

    public Integer getLicenseAllocated() {
        return licenseAllocated;
    }

    public String getComment() {
        return comment;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public String getContactName() {
        return contactName;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    /**
     * BUILDERS
     **/
    public License withId(String licenseId) {
        return new License(licenseId, this.productName, this.licenseType, this.organizationId, this.licenseMax, this.licenseAllocated, this.comment, organizationName, this.contactName, this.contactEmail, this.contactPhone);
    }

    public License withProductName(String productName) {
        return new License(this.licenseId, productName, this.licenseType, this.organizationId, this.licenseMax, this.licenseAllocated, this.comment, organizationName, this.contactName, this.contactEmail, this.contactPhone);
    }

    public License withLicenseType(String licenseType) {
        return new License(this.licenseId, this.productName, licenseType, this.organizationId, this.licenseMax, this.licenseAllocated, this.comment, organizationName, this.contactName, this.contactEmail, this.contactPhone);
    }

    public License withOrganizationId(String organizationId) {
        return new License(this.licenseId, this.productName, this.licenseType, organizationId, this.licenseMax, this.licenseAllocated, this.comment, organizationName, this.contactName, this.contactEmail, this.contactPhone);
    }


    // Transient fields
    public License withComment(String comment) {
        return new License(this.licenseId, this.productName, this.licenseType, this.organizationId, this.licenseMax, this.licenseAllocated, comment, organizationName, this.contactName, this.contactEmail, this.contactPhone);
    }

    public License withOrganizationName(String organizationName) {
        return new License(this.licenseId, this.productName, this.licenseType, this.organizationId, this.licenseMax, this.licenseAllocated, this.comment, organizationName, this.contactName, this.contactEmail, this.contactPhone);
    }

    public License withContactName(String contactName) {
        return new License(this.licenseId, this.productName, this.licenseType, this.organizationId, this.licenseMax, this.licenseAllocated, this.comment, this.organizationName, contactName, this.contactEmail, this.contactPhone);
    }

    public License withContactEmail(String contactEmail) {
        return new License(this.licenseId, this.productName, this.licenseType, this.organizationId, this.licenseMax, this.licenseAllocated, this.comment, this.organizationName, this.contactName, contactEmail, this.contactPhone);
    }

    public License withContactPhone(String contactPhone) {
        return new License(this.licenseId, this.productName, this.licenseType, this.organizationId, this.licenseMax, this.licenseAllocated, this.comment, this.organizationName, this.contactName, this.contactEmail, contactPhone);
    }

    public License withNewId() {
        return withId(UUID.randomUUID().toString());
    }

    public License withOrganization(Organization organization) {
        return withOrganizationName(organization.getName())
                .withContactName(organization.getContactName())
                .withContactEmail(organization.getContactEmail())
                .withContactPhone(organization.getContactPhone());
    }
}
