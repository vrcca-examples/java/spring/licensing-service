package com.thoughtmechanix.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Organization implements Serializable {
    private final String id;
    private final String name;
    private final String contactName;
    private final String contactEmail;
    private final String contactPhone;

    @JsonCreator
    public Organization(@JsonProperty("id") String id,
                        @JsonProperty("name") String name,
                        @JsonProperty("contactName") String contactName,
                        @JsonProperty("contactEmail") String contactEmail,
                        @JsonProperty("contactPhone") String contactPhone) {
        this.id = id;
        this.name = name;
        this.contactName = contactName;
        this.contactEmail = contactEmail;
        this.contactPhone = contactPhone;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getContactName() {
        return contactName;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public String getContactPhone() {
        return contactPhone;
    }
}
